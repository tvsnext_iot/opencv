import numpy as np
import math
import cv2
from shapely.geometry import MultiPoint
from shapely.geometry import Point
import _thread

import datetime

import boto3
import os
import boto
from boto.s3.key import Key
s3=boto3.client('s3')

import paho.mqtt.client as mqtt
#import boto3
import ssl
from time import sleep
#ls=time.time()
import _thread
client = boto3.client('iot')

def send_image1(threadName,img,status):
       
	folder='Region'+'/'
	print("Folder is created in expo.bucket:",folder)
	s3.put_object(Bucket='expo.bucket',Body='',Key=folder)
	s3.upload_file(img,'expo.bucket',folder+img)
 
	#mqtt_client.connect(awshost, awsport, keepalive=60)                                         
	#payload= "{\"state\":{\"reported\":{\"region\":\"" + status + "\" }}}"
	#mqtt_client.publish('$aws/things/Region/shadow/update',payload,0,True)
	os.remove(img)

def on_connect(client, userdata, flags, rc):
	global thing_name
	if rc==0:
	 	mqtt_client.subscribe('$aws/things/Region/shadow/update/delta',qos=0)
	
def on_message(client, userdata, msg):
    print("Message received : "+msg.topic+" | Qos: "+str(msg.qos)+" |   Date Received: "+str(msg.payload))

def update_shadow(threadName,status):

	mqtt_client.connect(awshost, awsport, keepalive=60)                                         
	payload= "{\"state\":{\"reported\":{\"region\":\"" + status + "\" }}}"
	mqtt_client.publish('$aws/things/Region/shadow/update',payload,0,True) 
	#payload= "{\"state\":{\"reported\":{\"memberscount_inside\":\"" + l10 + "\" ,\"membercount_outside\":\"" + l11 + "\"}}}"
	#mqtt_client.publish('$aws/things/Region/shadow/update',payload,0,True)
 
awshost = "AE2J99KNLEONC.iot.us-east-1.amazonaws.com"
awsport = 8883
clientId = "Region"
thingName = "Region"
caPath = "root-CA.crt"
certPath = "certificate.pem"
keyPath = "privatekey.pem"

mqtt_client = mqtt.Client(client_id='Region',clean_session=True)
mqtt_client.on_connect = on_connect
mqtt_client.on_message = on_message

mqtt_client.tls_set(caPath, certfile=certPath, keyfile=keyPath, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)


cap = cv2.VideoCapture(0)#sample-2.m4v
fgbg = cv2.createBackgroundSubtractorMOG2(history=0, varThreshold=50)
#fgbg = cv2.bgsegm.createBackgroundSubtractorMOG(history=150, backgroundRatio=0

#fourcc = cv2.VideoWriter_fourcc(*'XVID')
#out = cv2.VideoWriter('output.avi',fourcc, 20.0, (640,480))
font = cv2.FONT_HERSHEY_SIMPLEX
ix=-1
iy=-1
x1=0
y1=0
points=0
drawing=False
mode=True
roi=True
c=0
def draw_points(event,x,y,flags,param):
      
       global frame,ix,iy,x1,y1 ,drawing ,mode,points,c
       
       if event == cv2.EVENT_LBUTTONDOWN:
         drawing = True
         ix,iy = x,y
         q=np.array([(ix,iy)])
         if c==0:
          points=q
         #print("helllo")
         points=np.concatenate((points,np.array([(ix,iy)])))
         
         c=c+1
c1=0    
while True:
    ret, frame = cap.read()
    cv2.setMouseCallback("frame", draw_points)
    #print(points)
    #if c>3:
     #cv2.polylines(frame,np.int32([points]),True,(0,0,255),2)
    fgmask = frame
    fgmask = cv2.blur(frame, (10,10))
    fgmask = fgbg.apply(fgmask)
    fgmask = cv2.medianBlur(fgmask, 7)
    oldFgmask = fgmask.copy()
    image, contours, hierarchy = cv2.findContours(fgmask, cv2.RETR_EXTERNAL,1)
    for contour in contours:
        x,y,w,h = cv2.boundingRect(contour)
        if w>40 and h>60:
            cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),2, lineType=cv2.LINE_AA)
            point = (int(x+w/2.0), int(y+h/2.0))
            a = Point(point[0],point[1]).buffer(3)
            cv2.circle(frame, point, 3, (0,0,255),6)
            if c>3:
             poly = MultiPoint(points).convex_hull
             if poly.contains(a):
              c1=c1+1
               
              _thread.start_new_thread(update_shadow,("new","motion found")) 
              print(c1)
              timestamp = datetime.datetime.now()
              name=str(timestamp)+".jpg" 
              cv2.imwrite(name,frame)
              _thread.start_new_thread(send_image1,("new1",name,"motion found")) 
              c1=0 
              cv2.putText(frame,'motion',(0,50), font, 1,(255,255,255),2,cv2.LINE_AA)
    #cv2.imshow('a',oldFgmask)
    
    if c>3:
     #print(c)  
     cv2.polylines(frame,np.int32([points]),True,(0,0,255),2)
    #out.write(frame)
    cv2.imshow('frame',frame)
    q = cv2.waitKey(2) & 0x00ff
  
    if q == 27:
        break
    if q == ord('d'):
        points=np.array([(0,0),(0,0)])
        points=np.delete(points,[2,0])
        c=0 
        print("delete")
cap.release()
cv2.destroyAllWindows()