# import the necessary packages
import argparse
import warnings
import datetime
import imutils
import json
import time
import cv2
import os
import smtplib
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart

import boto3
import boto
from boto.s3.key import Key
s3=boto3.client('s3')

def SendMail(ImgFileName):
    img_data = open(ImgFileName, 'rb').read()
    From="mayurdodiya.blisslogix@gmail.com"
    To="dodiyamayur41@gmail.com"
    msg = MIMEMultipart()
    msg['Subject'] = 'Imageofmotion'
    msg['From'] = From
    msg['To'] = To

    text = MIMEText("test")
    msg.attach(text)
    image = MIMEImage(img_data, name=os.path.basename(ImgFileName))
    msg.attach(image)
    try:
     s = smtplib.SMTP("smtp.gmail.com", 587)
     s.ehlo()
     s.starttls()
     s.ehlo()
     s.login(From, "mayur@blisslogix")
     s.sendmail(From, To, msg.as_string())
     s.quit()
    except:
        return "failed to send mail"

    return "SMS sent to mail"


print(SendMail("Saturday 16 July 2016 06:50:11AM.jpg"))

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-c", "--conf", required=True,
	help="path to the JSON configuration file")
args = vars(ap.parse_args())
warnings.filterwarnings("ignore")
conf = json.load(open(args["conf"]))
client = None


cap=cv2.VideoCapture(0)

if cap.isOpened() < 0:
    print 'camera cannot be opened'
    quit()
cap.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH,320)
cap.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT,240)


print "[INFO] warming up..."
time.sleep(conf["camera_warmup_time"])
avg = None
lastUploaded = datetime.datetime.now()
motionCounter = 0

# capture frames from the camera
f=1
while True:
        ret,frame=cap.read()
	timestamp = datetime.datetime.now()
	text = "Unoccupied"

	# resize the frame, convert it to grayscale, and blur it
	frame = imutils.resize(frame, width=500)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (21, 21), 0)

	# if the average frame is None, initialize it
	if avg is None:
		print "[INFO] starting background model..."
		avg = gray.copy().astype("float")
		continue

	# accumulate the weighted average between the current frame and
	# previous frames, then compute the difference between the current
        # frame and running average 
	cv2.accumulateWeighted(gray, avg, 0.5)
	frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(avg))

	# threshold the delta image, dilate the thresholded image to fill
	# in holes, then find contours on thresholded image
	thresh = cv2.threshold(frameDelta, conf["delta_thresh"],255,cv2.THRESH_BINARY)[1]
	thresh = cv2.dilate(thresh, None, iterations=2)
	(cnts,_) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

	# loop over the contours
	for c in cnts:
		# if the contour is too small, ignore it
		if cv2.contourArea(c) < conf["min_area"]:
			continue

		# compute the bounding box for the contour, draw it on the frame,
		# and update the text
		(x, y, w, h) = cv2.boundingRect(c)
		cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
		text = "Occupied"

	# draw the text and timestamp on the frame
	ts = timestamp.strftime("%A %d %B %Y %I:%M:%S%p")
	cv2.putText(frame, "Room Status: {}".format(text), (10, 20),
		cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
	cv2.putText(frame, ts, (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX,
		0.5, (0, 0, 255), 1)

	# check to see if the room is occupied
	if text == "Occupied" or f:
                
		# check to see if enough time has passed between uploads
		if (timestamp - lastUploaded).seconds >= conf["min_upload_seconds"] or f:
			# increment the motion counter
			motionCounter += 1
			# check to see if the number of frames with consistent motion is
			# high enough
			if motionCounter >= conf["min_motion_frames"] or f:
				# check to see if dropbox sohuld be used
				if conf["use_aws"]:
					# write the image to temporary file
                                        tl = timestamp.strftime("%A %d %B %Y %I:%M:%S%p")
				        cv2.imwrite(tl+".jpg", frame)
					# upload the image to Dropbox and cleanup the tempory image
					print "[UPLOAD] {}".format(tl)
                                        folder='motion'+'/'
					print("Folder is created in client.bucket:",folder)
					s3.put_object(Bucket='expo.bucket',Body='',Key=folder) 
					s3.upload_file(tl+".jpg",'expo.bucket',tl+".jpg")
                                        print(SendMail(tl+".jpg"))					
					os.remove(tl+".jpg")
				# update the last uploaded timestamp and reset the motion
				# counter
				lastUploaded = timestamp
				motionCounter = 0
                                f=0 
	# otherwise, the room is not occupied
	else:
		motionCounter = 0
	# check to see if the frames should be displayed to screen
	if conf["show_video"]:
		# display the security feed
		cv2.imshow("Security Feed", frame)
		key = cv2.waitKey(1) & 0xFF
		# if the `q` key is pressed, break from the lop
		if key == ord("q"):
			break
	# clear the stream in preparation for the next frame
camera.release()
cv2.destroyAllWindows()